from troposphere.route53 import HostedZone, HostedZoneVPCs, HostedZoneConfiguration
from troposphere.route53 import GeoLocation, RecordSet, RecordSetGroup
from troposphere import Parameter, Ref, Output, GetAtt, Join, Template
import csv

class read_dns_csv(object):
    def __init__(self,csv_path):
        self.hosted_zones = {}
        self.read_csv_file(csv_path)

    def read_csv_file(self,file_path):
        with open(file_path, 'rb') as csvfile:
            dnsRecords = csv.reader(csvfile, delimiter=',')
            for row in dnsRecords:
                r0 = row[0].strip(' \t\n\r')
                r1 = row[1].strip(' \t\n\r')
                r2 = row[2].strip(' \t\n\r')
                r3 = row[3].strip(' \t\n\r')
                if r0[-1] != ".":
                    r0 = r0 + "."
                r_domain_name = r0.split(".")[-3] + "." + r0.split(".")[-2]
                sanitized = [r0,r1,r2,r3,False]
                if self.hosted_zones.get(r_domain_name) == None:
                    self.hosted_zones[r_domain_name]=[]
                for record_entry in self.hosted_zones[r_domain_name]:
                    if sanitized[0] == record_entry[0] and sanitized[2] == record_entry[2]:
                        record_entry[4] = True
                        sanitized[4] = True
                self.hosted_zones[r_domain_name].append(sanitized)

class SceptreResource(object):

    def __init__(self, sceptre_user_data):
        self.template = Template()
        self.sceptre_user_data = sceptre_user_data

def sceptre_handler(sceptre_user_data):
    return SceptreResource(sceptre_user_data).template.to_yaml()