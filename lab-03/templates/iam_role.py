from troposphere import GetAtt, Ref, Output
from troposphere.iam import Role
from sceptre_template import SceptreTemplate


class SceptreRole(SceptreTemplate):

    def __init__(self, sceptre_user_data):
        super(SceptreRole, self).__init__()
        self.sceptre_user_data = sceptre_user_data

def sceptre_handler(sceptre_user_data):
    return SceptreRole(sceptre_user_data).template.to_yaml()
