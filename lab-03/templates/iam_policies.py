from troposphere import Ref, Output
from troposphere.iam import ManagedPolicy
from sceptre_template import SceptreTemplate


class SceptreResource(SceptreTemplate):

    def __init__(self, sceptre_user_data):
        super(SceptreResource, self).__init__()
        self.sceptre_user_data = sceptre_user_data

def sceptre_handler(sceptre_user_data):
    return SceptreResource(sceptre_user_data).template.to_yaml()
